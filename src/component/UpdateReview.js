import { useEffect, useState } from 'react'
import axios from 'axios'
import './updateReview.css'
import { Link, useHistory } from 'react-router-dom'
import ReviewProfile from './ReviewProfile'
import Rating from 'react-simple-star-rating'


const UpdateReview = (props) => {

    const [tesDisplay, setTesDisplay] = useState([])

    const [movie, setMovie] = useState(0)
    const [dropdown, setDropdown] = useState(tesDisplay.rating)
    const [review, setReview] = useState(tesDisplay.comment)
    const [backSummon, setBackSummon] = useState('')
    const [isLoading, setIsLoading] = useState(false)
    const [reviews, setReviews] = useState([tesDisplay])


    const handlerClick = (e) => {
        console.log(rating)
        console.log(review)
        const bodyData = {
            rating: rating,
            review: review
        }

        setIsLoading(true)
        axios(`https://team-f-be-binar8.nandaworks.com/review?id=${localStorage.getItem('reviewId')}`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }, data: JSON.stringify(bodyData)
        })
        .then(() => {
                setBackSummon('come')
                setIsLoading(false)})
    }

    const handlerChange = (e) => {
        setReview(e.target.value)
    }

    const handlerDropdown = (e) => {
        setDropdown(e.target.value)
    }

    const [submenu, setSubmenu] = useState('update')

    useEffect(() => {
        setIsLoading(true)
        axios(`https://team-f-be-binar8.nandaworks.com/review`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                "userId": localStorage.getItem('id')
            },
        })
            .then((res) => {
                setTesDisplay(res.data)
                setIsLoading(false)
            })
        axios(`https://team-f-be-binar8.nandaworks.com/movie/details?id=${localStorage.getItem('mananich')}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
        })
            .then((res) => {
                setMovie(res.data[0])
                setIsLoading(false)
            })

    }, [])


    const [rating, setRating] = useState(tesDisplay.rating)
    const handleRating = (rate) => {
        setRating(rate)
        setReview()
    }

    let history = useHistory()
    const handleDeleteFinal = (e) => {
        setIsLoading(true)
        axios(`https://team-f-be-binar8.nandaworks.com/review?id=${e.target.value}`, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
        })
        .then(() => {
            setSubmenu('review')
        })
        .then(axios(`https://team-f-be-binar8.nandaworks.com/review`, {
            method: "GET",
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${localStorage.getItem('token')}`,
              "userId": localStorage.getItem('id')
            },
          })
            .then((res) => {
              setTesDisplay(res.data)
              setIsLoading(false)
              console.log(res.data)
            }))
    }


    if (isLoading === true) {
        return (
            <div className="spinner">
                <div className="double-bounce1"></div>
                <div className="double-bounce2"></div>
            </div>
        )
    } else {
        if (submenu === 'review') {
            return <ReviewProfile />
        } else {
            return (
                <div className='updateReview'>
                    {tesDisplay.filter(filteredRev => filteredRev.id === localStorage.getItem('reviewId')).map((reviewed) => (
                        <div>
                            <h3>Update Your Review</h3>
                            <div className="container">
                                <div className="reviewBox finalDeletion">
                                    <div className="deleteFinal">
                                        <button value={reviewed.id} onClick={handleDeleteFinal}>Delete This Review</button>
                                    </div>

                                    <h1>{movie.title}</h1>
                                    <form>

                                        <label htmlFor="rating">Rating: <div className='App'>
                                            <Rating
                                                onClick={handleRating}
                                                ratingValue={rating}
                                                size={40}
                                                label
                                                stars={10}
                                                transition
                                                fillColor='white'
                                                emptyColor='gray'
                                                className='foo'
                                            />
                                        </div></label><br />
                                        <label htmlFor="comment">Review</label><br />
                                        <textarea name="comment" id="comment" cols="60" rows="8" defaultValue={reviewed.review} onChange={(e) => handlerChange(e, 'value')}></textarea>
                                        <button className={`editIcon btn-secondary ${backSummon !== 'come' ? 'save-active' : ''}`} type='button' onClick={handlerClick}>{backSummon === 'come' ? 'Updated!' : 'Save'}</button>
                                        <button className={`backIcon ${backSummon === 'come' ? 'back-active' : ''}`} type='submit'>Back</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    ))}

                </div>
            );
        }

    }

}

export default UpdateReview;