import { useHistory } from "react-router-dom";
import './Logout.css'



const Logout = () => {

    const history = useHistory()
    const handleOnClick = () => {
        localStorage.clear();
        history.push('/')
    }
    
    return (
        <div>
            {/* {localStorage.getItem('token') !==} */}
            <button className='signUpButton logout' onClick={e => handleOnClick(e, 'value')}>Logout</button>
        </div>
    );
}
 
export default Logout;