import React, { useState, useEffect } from "react";
import {
  Card,
  CardBody,
  CardImg,
  Col,
  Container,
  Row,
} from "reactstrap";
import axios from "axios";

function TmdbHooks() {
  const imgUrl = "https://image.tmdb.org/t/p/w500";
  const apiUrl = "https://api.themoviedb.org/3/";

  const [movies, setMovies] = useState([]);

  useEffect(() => {

    axios
      .get(
        `${apiUrl}discover/movie?api_key=8508a0bd1efc493c4bfa095b6a37f250&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_genres=12`
      )
      .then((res) => {
        setMovies(res.data.results);
      });

  }, []);

  return (
    <div>
      <Container>

        <Row>
          {movies.length !== 0 ? (
            movies.map((movie) => (
              <Col key={movie.id} md={3}>
                <Card
                  style={{
                    marginBottom: "16px",
                  }}
                >
                  <CardImg src={`${imgUrl}${movie.poster_path}`} />
                  <CardBody
                    style={{
                      minHeight: "200px",
                    }}
                  >
                    <h2>{movie.title}</h2>
                    <p>{movie.release_date}</p>
                  </CardBody>
                </Card>
              </Col>
            ))
          ) : (
            <div>Loading...</div>
          )}
        </Row>
      </Container>
    </div>
  );
}

export default TmdbHooks;