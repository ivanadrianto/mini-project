import React, { useState, useEffect } from "react";
import fotoprofil from "../img/profilemockup.jpg";
import { Container } from "reactstrap";
import "./Reviews.css"
import Axios from "axios";
import { useParams } from "react-router-dom";
import Rating from 'react-simple-star-rating'


const Reviews = () => {
    // class Reviews extends Component {
    const [reviews, setReviews] = useState([]);
    const [rating, setRating] = useState(0);
    const [comment, setComment] = useState("");
    const [isLogin, setIsLogin] = useState(false)
    const [isLoading, setisLoading] = useState(false)
    const params = useParams()
    console.log(params)


    const handleSubmitReview = (e) => {

        e.preventDefault()

        const url = "https://team-f-be-binar8.nandaworks.com/review"
        const bodyData = {
            rating: rating,
            review: comment,
            movieId: params.id,
            impression: ''
        }

        fetch(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }, body: JSON.stringify(bodyData)
        })
            .then(()=>getDataReviews())
    }

    useEffect(() => {
        getDataReviews();

    }, [localStorage.getItem('reviewTrig')])

    const getDataReviews = () => {
        setisLoading(true)
        Axios
            .get(`https://team-f-be-binar8.nandaworks.com/review`)
            .then((res) => {
                setReviews(res.data);
                setisLoading(false)
            })
    };
    const [review, setReview] = useState('')
    const handleRating = (rate) => {
        setRating(rate)
    }

    


    if (localStorage.getItem('token') !== null) {
        return <Container>
            <div class="container">
                <div className='row'>
                    <div className='col-2'>
                        <div id="fotoprofilReview">
                            <img src={fotoprofil} alt="profil" />
                        </div>
                    </div>
                    <div className='col-10'>
                        <div class='commentColumn'>
                            <form onSubmit={handleSubmitReview}>
                                <h4>{localStorage.getItem('fullName')}</h4>

                                <label htmlFor="rating">Rating: <br /><div className='App'>
                                    <Rating
                                        onClick={handleRating}
                                        ratingValue={rating}
                                        size={40}
                                        label
                                        stars={10}
                                        transition
                                        fillColor='#fe024e'
                                        emptyColor='gray'
                                        className='foo'
                                    />
                                </div></label><br /><br />

                                <textarea id="textareaReview" rows="10" cols="200" form="input" onChange={(e) => setComment(e.target.value)} >
                                    Input your review here</textarea>
                                <br />
                                <button id="reviewButton" type="submit" className="btn">Submit Review</button>
                            </form>
                        </div>
                    </div>
                </div>
                <hr />

                {reviews.filter(review => review.movieId === params.id).map(review =>
                    <div className='row' style={{ minHeight: "200px" }} >
                        <div className='col-2'>
                            <div id="fotoprofilReview">
                                <img src={fotoprofil} alt="profil" />
                            </div>
                        </div>
                        <div className='col-10'>
                            <div class='commentColumn'>
                                <div>
                                    <h4>{review.userName}</h4>
                                    <label htmlFor="rating"><div className='App'>
                                        <Rating
                                            onClick={handleRating}
                                            ratingValue={review.rating}
                                            size={20}
                                            label
                                            stars={1}
                                            // transition
                                            fillColor='#fe024e'
                                            emptyColor='gray'
                                            className='foo'
                                        />
                                    </div></label>
                                    <p>{review.review}</p>
                                    <hr />
                                </div>
                            </div>
                        </div>
                    </div>)}
            </div>



        </Container>
    } else {
        return (
            <Container>
                <div class="container">
                    <br /><br /><br />
                    <h3>Log in/Sign up to add your review</h3>
                    <br /><br /><br />
                    {reviews.filter(review => review.movieId === params.id)
                    .map(review =>
                        <div className='row' style={{ minHeight: "200px" }} >
                            <div className='col-2'>
                                <div id="fotoprofilReview">
                                    <img src={fotoprofil} alt="profil" />
                                </div>
                            </div>
                            <div className='col-10'>
                                <div class='commentColumn'>
                                    <div>
                                        <h4>{review.userName}</h4>
                                        <label htmlFor="rating"><div className='App'>
                                            <Rating
                                                onClick={handleRating}
                                                ratingValue={review.rating}
                                                size={40}
                                                label
                                                stars={10}
                                                fillColor='#fe024e'
                                                emptyColor='gray'
                                                className='foo'
                                            />
                                        </div></label>
                                        <p>{review.review}</p>
                                        <hr />
                                    </div>




                                </div>
                            </div>
                        </div>)}
                </div>



            </Container>
        )
    }
}

export default Reviews;
