import Watchlist from "./Watchlist";
import axios from 'axios'
import { useState, useEffect } from 'react'
import { Link } from "react-router-dom";
import star from '../img/star.svg'

const WatchlistMov = (props) => {
    const [watchlists, setWatchlists] = useState('')
    const [isLoading, setisLoading] = useState(false)

    useEffect(() => {
        setisLoading(true)
        axios(`https://team-f-be-binar8.nandaworks.com/movie/details?id=${props.movieId}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
        })
            .then((res) => {
                setWatchlists(res.data[0])
                setisLoading(false)
            })

    }, [])
    return (
        <div>
            {isLoading === true ?
                <div className="loading-icon spinner">
                    <div className="double-bounce1"></div>
                    <div className="double-bounce2"></div>
                </div> :

                <div>
                    <div className="watchcard">
                        <Link to={`/movie-details/${props.movieId}`}>
                            <img src={`${watchlists.poster}`} alt="poster" />
                        </Link>
                        <div className="star-rating">
                            <img src={star} alt="star" />
                            <h2>: {watchlists.rating}</h2>
                        </div>
                        <h1>{watchlists.title}</h1>
                    </div>
                </div>}
        </div>

        // <div>
        //     <img src={`http://image.tmdb.org/t/p/w185/${watchlists.poster_path}`} alt="poster"/>
        //     <h1>{watchlists.original_title}</h1>
        // </div>

    );
}

export default WatchlistMov;