import React, { Component } from "react";
import logo1 from "../img/milan1.png";
import logo2 from "../img/milan2.png";
import "./navbar.css";
import profile from "../img/profilemockup.jpg";
import { Link } from 'react-router-dom'
import search from '../img/search.svg'
import { withRouter } from 'react-router-dom'
import SignUp from "./SignUp";
import Logout from "./Logout";



class Navbar extends Component {

  constructor() {
    super()
    this.state = {
      searched: '',
      menu: '',
      isLoggedIn: false
    }
  }

  componentDidMount() {
    if (localStorage.getItem('token') !== null) {
      this.setState({
        isLoggedIn: true
      })
    } else {
      this.setState({
        isLoggedIn: false
      })
    }
  }

  componentDidUpdate() {
    if (this.state.isLoggedIn === false && localStorage.getItem('token') !== null) {
      this.setState({
        isLoggedIn: true
      })
    } else if (this.state.isLoggedIn === true && localStorage.getItem('token') === null){
      this.setState({
        isLoggedIn: false
      })
    }
  } 

  handlerChange = (e) => {
    this.setState({
      searched: e.target.value
    }
    )
  }

  handleKeyPress(target, value) {
    const { history } = this.props;
    if (target.charCode == 13) {
      history.push(`/search/${this.state.searched}`);
    }
  }

  handlerSubmit = (event) => {
    event.preventDefault();
    this.handleClick()
    this.props.history.push(`/search/${this.state.searched}`)
  }

  handleClick = () => {
    localStorage.setItem('search', this.state.searched)
  }

  profileClick = () => {
    let username = localStorage.getItem('username')
    this.props.history.push(`/profile/${username}`);
  }


  render() {
    return (
      <div> {this.state.isLoggedIn === false ?
        <div className='navigation' expand="md">
          <Link exact to='/' >
            <div className="logoHeader">
              <img src={logo1} alt="logo1" className="logo1" />
              <img src={logo2} alt="logo2" className="logo2" />
            </div>
            <h1>MilanTV</h1>
          </Link>

          <form onSubmit={(event) => this.handlerSubmit(event)} >
            <input onChange={(e) => this.handlerChange(e, 'value')} className="navbar-input" placeholder="Search" aria-label="Search"
            />
          </form>
          <Link to='/search' >

            <div className="searchIcon">

              <img onClick={this.handleClick} src={search} alt="search" />
            </div>
          </Link>
          <SignUp />
        </div> :


        <div className='navigation' expand="md">
          <Link exact to='/' >
            <div className="logoHeader">
              <img src={logo1} alt="logo1" className="logo1" />
              <img src={logo2} alt="logo2" className="logo2" />
            </div>
            <h1>MilanTV</h1>
          </Link>

          <form onSubmit={(event) => this.handlerSubmit(event)} >
            <input onChange={(e) => this.handlerChange(e, 'value')} className="navbar-input" placeholder="Search" aria-label="Search"
            />
          </form>
          <Link to='/search' >

            <div className="searchIcon">
              <img onClick={this.handleClick} src={search} alt="search" />
            </div>
          </Link>
          <Link >
            <img src={profile} alt="profil" className='profil' onClick={this.profileClick}></img>
          </Link>
          {/* <div class="dropdown">
            <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img src={profile} alt="profil" className='profil' onClick={this.profileClick} id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></img></a>

            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <a class="dropdown-item" href="#">Something else here</a>
            </div>
          </div> */}
          <SignUp />
          <Logout />

        </div>}

      </div>

    );
  }
}

export default withRouter(Navbar);