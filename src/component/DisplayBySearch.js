import { useEffect, useState } from "react";
import Footer from './Footer'
import React, { Component } from 'react';
import './displayBySearch.css';
import { useHistory, useParams } from 'react-router-dom';
import star from '../img/star.svg'
import Navbar from './Navbar'



const DisplayBySearch = (props) => {

    const [movies, setMovies] = useState([])
    const [isLoading, setIsLoading] = useState(false)
    const [currentPage, setCurrentPage] = useState(1)
    const [moviesPerPage, setMoviesPerPage] = useState(5)
    const history = useHistory();
    const params = useParams()

    const inputan = localStorage.getItem('search')

    useEffect(() => {
        // inputan === '' ? localStorage.setItem('search', 'akjsfhaidyiohakjjfyhodjaskdtipdhdxodajdgoidh') :
        console.log(params)
        setIsLoading(true)
        fetch(`https://team-f-be-binar8.nandaworks.com/movie/homes?title=${params.keyword}`)
            .then(response => response.json())
            .then(res => {
                
                setMovies(res)
                setIsLoading(false)
            })

    }, [params.keyword])
    console.log(params)
    return (

        <div>
            <Navbar />
            {inputan === 'akjsfhaidyiohakjjfyhodjaskdtipdhdxodajdgoidh' ?
                <div className='container'>
                    <br /><br /><br />
                    <h4>Enter keyword to start searching</h4>
                </div> :

                isLoading === true ?
                    <div className="loading-icon spinner">
                        <div className="double-bounce1"></div>
                        <div className="double-bounce2"></div>
                    </div> :
                    <div className="searchMania container">
                        <h4>Displaying search results for {localStorage.getItem('search')}</h4>
                        <div className="row">
                            {movies.map(movie =>
                                <div className='searched col-lg-3 col-sm-12 col-md-6' >
                                    <div className=" searchbox mx-1 my-2 px-0">
                                        <img onClick src={movie.poster} alt='poster' value={movie.id} onClick={() => { history.push(`/movie-details/${movie.id}`); }} />
                                        <h1 value={movie.movieId}>{movie.title}</h1>

                                    </div>

                                </div>
                            )
                            }

                        </div>
                        <div className="pager">
                        </div>


                    </div>

            }
            <Footer />

        </div>

    );
}



export default DisplayBySearch;