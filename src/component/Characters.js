import React, { Component } from "react";
import "./character.css";

const Characters = (props) => {
  return (
    <div className="container">
      <div className="row">
        {props.characters.map((character) => (
          <div className="character col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3 align-items-center">
            <div id="char-card">
              <img
                src={character.characterPicture}
                alt=""
              />
              <h1>{character.characterName}</h1>
              <h2>as {character.characterAs}</h2>
            </div>

            <hr />
          </div>
        ))}
        ;
      </div>
    </div>
  );
};

export default Characters;
