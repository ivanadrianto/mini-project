import { useEffect, useState } from 'react'
import './movieOfReviewP.css'
import axios from 'axios'



const MovieOfReviewP = (props) => {
    const [title, setTitle] = useState('')
    useEffect(() => {
        axios(`https://team-f-be-binar8.nandaworks.com/movie/details?id=${props.movie}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
        })
            .then((res) => {
                setTitle(res.data[0])
            })

    }, [])

    return (
        <div className='fromAnotherAPI'>
            <h5>{title.title} </h5>
            <img src={title.poster} alt="movie pic" id='movieOther' />
        </div>

    );
}

export default MovieOfReviewP;