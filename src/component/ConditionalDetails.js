import React, { Component } from "react";
import Navbar from "./Navbar";
import Footer from "./Footer";
import "./MovieDetails.css";
import { withRouter } from "react-router-dom";
import Characters from "./Characters";
import { Link, Switch, Route } from "react-router-dom";
import Profil from './Profile'
import axios from "axios";
import Reviews from './Reviews'
import Overview from './Overview'
import StarRatings from 'react-star-ratings'
import star from '../img/star.svg'
import chatbox from '../img/chat.svg'


class MovieDetails extends Component {
    constructor() {
        super();
        this.state = {
            data: '',
            isLoading: false,
            characterList: [],
            submenu: 'overview',
            watch: false
        };
    }

    componentDidMount() {
        const id = this.props.match.params.id;
        console.log(id)

        axios(`https://team-f-be-binar8.nandaworks.com/movie/character?movieId=${id}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
        })
            .then((res) => {
                console.log(res)
                this.setState({
                    characterList: res.data,
                    isLoading: false
                })
            })

        axios(`https://team-f-be-binar8.nandaworks.com/movie/detail?id=${id}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                // Authorization: `Bearer ${localStorage.getItem('token')}`
            },
        })
            .then((res) => {
                this.setState({
                    data: res.data[0]
                })
            })

    }

    handleInput(e) {
        this.setState({
            submenu: e.target.value
        });
    }

    handlerClick(e) {
        const id = this.props.match.params.id;
        axios({
            method: 'post',
            url: 'https://team-f-be-binar8.nandaworks.com/watchlists',
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            data: {
                movieId: id
            }
        });
        
        
        this.setState({
            watch: true
        })
    }



    render() {
        const id = this.props.match.params.id
        const coba1 =
            <div>
                <Navbar />
                <section
                    id="home"
                    className="banner d-flex position-relative cover hero"
                    style={{
                        backgroundImage: `url(${this.state.data.poster})`,
                    }}>
                    <div className="movie-details container-sm container-md container-lg container-xl container">
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">
                                <h1 className="text-white teks-banner">
                                    {this.state.data.title}
                                </h1>
                                <div className="glimpse">
                                    <h4> <img className='rating-icon' src={star} alt="star" />: {this.state.data.rating} /10</h4>
                                    <h5> <img className='comment-icon' src={chatbox} alt="" />
                                        {this.state.data.totalReview} reviews</h5>
                                </div>

                            </div>
                            <p className="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 description">{this.state.data.synopsys}</p>
                            <div className="col-12 action-button">
                                <a
                                    className="trailer btn bg-red text-white mr-2 font-weight-bold"
                                    href={this.state.data.trailer}
                                    role="button"
                                >
                                    <span>Watch Trailer</span>
                                </a>
                                {localStorage.getItem('token') !== null ? 
                                <a
                                className={`watchlist btn bg-red text-white font-weight-bold ${this.state.watch === true ? 'watchlist-active' : ''}${localStorage.getItem('token'===null) ? 'watchlist-nonactive' : ''}`}
                                value={this.props.match.params.id}
                                role="button" onClick={(e) => this.handlerClick(e, 'value')}
                            >
                                {this.state.watch == true ? <span>Added to Your Watchlist!</span> : <span>Add to Watchlist</span>}
                            </a> : ''}
                                
                            </div>
                        </div>
                    </div>
                </section>
                <div className="mau-apa container">
                    <button className={`submenu btn ${this.state.submenu === 'overview' ? 'submenu-active' : ''}`} value='overview' onClick={(e) => this.handleInput(e, 'value')}>Overview</button>
                    <button className={`submenu btn ${this.state.submenu === 'characters' ? 'submenu-active' : ''}`} value='characters' onClick={(e) => this.handleInput(e, 'value')}>Characters</button>
                    <button className={`submenu btn ${this.state.submenu === 'reviews' ? 'submenu-active' : ''}`} value='reviews' onClick={(e) => this.handleInput(e, 'value')}>Reviews</button>
                </div>
            </div>
        if (this.state.submenu === 'characters') {
            return <div>
                {coba1}
                <Characters characters={this.state.characterList} />
            </div>
        } else if (this.state.submenu === 'overview') {
            return <div>
                {coba1}
                <Overview overviews={this.state.data} />
            </div>
        } else {
            return <div>
                {coba1}
                <Reviews />
            </div>
        }
    }
}

export default withRouter(MovieDetails);
