import axios from "axios";
import { useEffect, useState } from "react";
import WatchlistMov from './WatchlistsMov'
import './watchlist.css'
import remove from '../img/delete.svg'
import { useParams } from 'react-router-dom'



const Watchlist = () => {
    const [movies, setMovies] = useState([]);
    const [isLoading, setIsLoading] = useState(false)

    const id = useParams().id

    useEffect(() => {

        setIsLoading(true)
        axios(`https://team-f-be-binar8.nandaworks.com/watchlists?userId=${localStorage.getItem('id')}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
        })
            .then((res) => {
                setMovies(res.data)
                setIsLoading(false)
            })
    }, [])

    const deleteData = (e, id) => {
        console.log(id)
        setIsLoading(true);
        axios(`https://team-f-be-binar8.nandaworks.com/watchlists?id=${e.target.value}`, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
        })
            .then(() => {
                setMovies([...movies.filter(movie => movie.id !== e.target.value)])
                setIsLoading(false);
            })
    }



    return (
        <div className='container'>
            <div className="row watchlist">
                {isLoading && <div className="loading-icon spinner">
                    <div className="double-bounce1"></div>
                    <div className="double-bounce2"></div>
                </div>}
                {!isLoading &&
                    movies.map(movie =>
                        <div className="col-lg-3 col-sm-6 col-md-6 mx-3 d-flex justify-content-center">
                            <div className='icon'>
                                <button value={movie.id} onClick={(e) => deleteData(e, 'value')}>Delete</button>
                            </div>
                            <WatchlistMov movieId={movie.movieId} />

                        </div>
                    )}

            </div>

        </div>
    );
}

export default Watchlist;