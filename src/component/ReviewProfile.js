import { useEffect, useState } from 'react';
import MovieOfReviewP from './MovieOfReviewP';
import './reviewProfile.css'
import UpdateReview from './UpdateReview';
import axios from 'axios'


const ReviewProfile = (props) => {


  const [reviews, setReviews] = useState([])
  const [loading, setIsLoading] = useState(false)
  useEffect(() => {
    setIsLoading(true)
    axios(`https://team-f-be-binar8.nandaworks.com/review`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        "userId": localStorage.getItem('id')
      },
    })
      .then((res) => {
        setReviews(res.data)
        setIsLoading(false)
        console.log(res.data)
      })
  }, [])

  const [reviewSubmenu, setReviewSubmenu] = useState('review')

  const handleClick = (e) => {
    setReviewSubmenu('editReview')
    localStorage.setItem('mananich', `${e.target.value}`)
    localStorage.setItem('reviewId', `${e.target.id}`)
    localStorage.setItem('reviewSubmenu', 'editReview')
  }



  if (reviewSubmenu === 'review') {
    console.log(reviews)
    return (
      <div className="container">
        <div className="row">
          {reviews.filter(review => review.userId === localStorage.getItem('id'))
          .map((reviewed) => (
            <div className="reviewBox my-3">
              <div className='theAncientOnes'>
                <MovieOfReviewP movie={reviewed.movieId} />
              </div>
              <div className="reviewData">
                <button id={reviewed.id} value={reviewed.movieId} onClick={(e) => handleClick(e, 'value')} className='editIcon btn-secondary mx-2'>Edit/Delete</button>
                <div className="container">
                  <h3>Rating: <p>{reviewed.rating}</p></h3>
                  <h3>Review:
                <br /><p>"{reviewed.review}"</p></h3>
                </div>

              </div>
            </div>
          ))}
          
        ;
      </div>
      </div>
    )
  } else {
    return (
      <div className="container">
        <UpdateReview />
      </div>
    );
  }

}

export default ReviewProfile;