import React, { Component } from 'react'
import { Route, Switch, NavLink, Link } from 'react-router-dom'
import Navbar from './component/Navbar'
import Footer from './component/Footer'
import Profile from './component/Profile'
import SignUp from './component/SignUp'
import ConditionalDetails from './component/ConditionalDetails'
import DisplayBySearch from './component/DisplayBySearch'
import Homepage from './pages/Homepage'




class App extends Component {
  render() {
    return (
      <div>
        <Switch>
          <Route exact path='/'>
            <Homepage />
          </Route>
          <Route path="/profile/">
            <Profile />
          </Route>
          <Route path='/search/:keyword'>
            <DisplayBySearch />
          </Route>
          <Route exact path="/movie-details/:id">
            <ConditionalDetails />
          </Route>
        </Switch>

      </div>

    )
  }
}

export default App