import React, { useState, useEffect } from "react";
import { useHistory, Switch, Route } from "react-router-dom";
import { Button, Card, CardBody, CardImg, Col, Container, Row } from "reactstrap";
import Pagination from "react-js-pagination";
import axios from "axios";
import "./style.css";

function Movies() {
  const apiUrl = "https://team-f-be-binar8.nandaworks.com/";

  const history = useHistory();
  const [activePage, setActivePage] = useState(1);
  const [movies, setMovies] = useState([]);
  const [genres, setGenres] = useState([]);
  const [currentGenre, setCurrentGenre] = useState(null);


  useEffect(() => {
    axios
      .get(
        `${apiUrl}movies/home?page=1&limit=10`
      )
      .then((res) => {
        console.log(res.data.movies);
        setMovies(res.data.movies);
      });

    axios
      .get(
        `${apiUrl}movies/home?page=1&limit=10`
      )
      .then((res) => {
        console.log(res.data.movies)
        setGenres(res.data.movies)
      });
  }, []);

  const getMoviesByGenre = (gen) => {
    console.log(gen)
    setCurrentGenre(gen);
    axios
      .get(
        `${apiUrl}movie/homee?genre=${gen}`
      )
      .then((res) => {
        console.log(res.data)
        setMovies(res.data)
      });
  };

  const handlePageChange = (page) => {
    console.log(page);
    const urlAPI = `${apiUrl}movies/home?page=${page}&limit=10`;
    getData(urlAPI);
    setActivePage(page);
  };

  const getData = (url) => {
    axios
      .get(url)
      .then((res) => {
        console.log(res.data.movies)
        setMovies(res.data.movies);
      })
      .catch((err) => console.log(err));
  };

  const [category, setCategory] = useState('all')

  const allCategory = (e) => {
    axios.get(`${apiUrl}movies/home?page=1&limit=10`)
      .then(res => setMovies(res.data.movies))
      .then(setCategory('all'))
  }


  const categoryChange = (e) => {
    axios.get(`${apiUrl}movie/homee?genre=${e.target.value}`)
      .then(res => setMovies(res.data))
      .then (()=> setCategory(e.target.value))
  }
  return (
    <div>
      <Container>
        <h2>Browse by Category</h2>
        <button className={`catbutton ${category === 'all' ? 'catbutton-active' : ''}`} onClick={e => categoryChange(e, 'value')} value='all' onClick={e => allCategory(e, 'value')}>All</button>
        <button value='action' className={`catbutton ${category === 'action' ? 'catbutton-active' : ''}`} onClick={e => categoryChange(e, 'value')}>Action</button>
        <button value='comedy' className={`catbutton ${category === 'comedy' ? 'catbutton-active' : ''}`}  onClick={e => categoryChange(e, 'value')}>Comedy</button>
        <button value='mystery' className={`catbutton ${category === 'mystery' ? 'catbutton-active' : ''}`}  onClick={e => categoryChange(e, 'value')}>Mystery</button>
        <button value='fantasy' className={`catbutton ${category === 'fantasy' ? 'catbutton-active' : ''}`}  onClick={e => categoryChange(e, 'value')}>Fantasy</button>
        <button value='musical' className={`catbutton ${category === 'musical' ? 'catbutton-active' : ''}`}  onClick={e => categoryChange(e, 'value')}>Musical</button>
        <button value='animation' className={`catbutton ${category === 'animation' ? 'catbutton-active' : ''}`}  onClick={e => categoryChange(e, 'value')}>Animation</button>
        <button value='drama' className={`catbutton ${category === 'drama' ? 'catbutton-active' : ''}`}  onClick={e => categoryChange(e, 'value')}>Drama</button>
        <button value='horror' className={`catbutton ${category === 'horror' ? 'catbutton-active' : ''}`}  onClick={e => categoryChange(e, 'value')}>Horror</button>
        <button value='romance' className={`catbutton ${category === 'romance' ? 'catbutton-active' : ''}`}  onClick={e => categoryChange(e, 'value')}>Romance</button>
        <button value='crime' className={`catbutton ${category === 'crime' ? 'catbutton-active' : ''}`}  onClick={e => categoryChange(e, 'value')}>Crime</button>
        <button value='adventure' className={`catbutton ${category === 'adventure' ? 'catbutton-active' : ''}`}  onClick={e => categoryChange(e, 'value')}>Adventure</button>
        <button value='sci-fi' className={`catbutton ${category === 'sci-fi' ? 'catbutton-active' : ''}`}  onClick={e => categoryChange(e, 'value')}>Sci-Fi</button>







        <Row>
          {
            movies.map((movie) => (
              <Col key={movie.title} md={3}>
                <Card id="cursor"
                  className="kartu"
                  onClick={() => {
                    history.push(`/movie-details/${movie.movieId}`);
                  }}
                >
                  <CardImg src={`${movie.poster}`} />
                  <CardBody id="bodi">

                    <h2>{movie.title}</h2>
                    <p>{movie.releaseDate}</p>

                  </CardBody>
                </Card>
              </Col>
            ))
          }
        </Row>
        <div className="pagi">
          <Pagination
            id="pag"
            className="pagin"
            activePage={activePage}
            totalItemsCount={20}
            onChange={handlePageChange}
            itemsCountPerPage={10}
            innerClass="pagination pagination-sm"
            prevPageText="Previous"
            nextPageText="Next"
            itemClass="page-item"
            linkClass="page-link"
          />
        </div>


      </Container>
    </div>
  );
}

export default Movies;
